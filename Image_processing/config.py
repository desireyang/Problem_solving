# -*- coding utf-8 -*-
# @Time    : 2020/7/21 22:27
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : config.py
# Software : PyCharm
# Explain  : 因为百度AI平台提供了多种图像处理方式，请求URL主体都相同，但是传参不同，所以我们需要对不同的处理方式进行参数处理

def get_config():
    img_before = input("请输入当前文件夹下需要处理的图片名称：")
    process_action = ['', 'selfie_anime', 'colourize', 'style_trans']
    print("支持以下处理动作：\n1:为人像动漫化\n2:图像上色\n3:为图像风格化")
    # 处理动作： selfie_anime 为人像动漫化，colourize 图像上色，style_trans 为图像风格化
    i = int(input("请输入需要处理的动作："))
    """
    cartoon：卡通画风格
    pencil：铅笔风格
    color_pencil：彩色铅笔画风格
    warm：彩色糖块油画风格
    wave：神奈川冲浪里油画风格
    lavender：薰衣草油画风格
    mononoke：奇异油画风格
    scream：呐喊油画风格
    gothic：哥特油画风格"""
    others = ['', 'cartoon', 'pencil', 'color_pencil', 'warm', 'wave', 'lavender', 'mononoke', 'scream', 'gothic']
    j = 0
    if process_action[i] == 'style_trans':
        print("支持转化的风格有：\n\
                1：卡通画风格\n\
                2：铅笔风格\n\
                3：彩色铅笔画风格\n\
                4：彩色糖块油画风格\n\
                5：神奈川冲浪里油画风格\n\
                6：薰衣草油画风格\n\
                7：奇异油画风格\n\
                8：呐喊油画风格\n\
                9：哥特油画风格\n")
        j = int(input("请输入需要转化的风格类型（数字）："))
    return img_before, process_action[i], others[j]
