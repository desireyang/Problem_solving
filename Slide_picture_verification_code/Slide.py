# -*- coding utf-8 -*-
# @Time    : 2020/6/21 15:28
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : Slide.py
# Software : PyCharm
# Explain  : 
# pip install -U opencv-python
# 用来处理图像
import cv2


def find_pic(target='./img/target.png', template='./img/template.png'):
    """
    找到图像中最佳哦哦诶位置
    :param target: 目标即背景图
    :param template: 模板即需要找到的图
    :return: 返回最佳匹配及最差匹配和对应的坐标
    """
    # 读取图片
    target_rgb = cv2.imread(target)
    # 灰度处理
    target_gray = cv2.cvtColor(target_rgb, cv2.COLOR_BGR2GRAY)
    # 读取模块图片
    template_rgb = cv2.imread(template, 0)
    # 匹配模块位置
    res = cv2.matchTemplate(target_gray, template_rgb, cv2.TM_CCOEFF_NORMED)
    # 获取最佳与最差匹配
    value = cv2.minMaxLoc(res)
    print(value)
    # 返回最佳匹配的X坐标
    return value[2][0]
