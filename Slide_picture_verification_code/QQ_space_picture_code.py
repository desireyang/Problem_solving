# -*- coding utf-8 -*-
# @Time    : 2020/6/21 14:37
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : QQ_space_picture_code.py
# Software : PyCharm
# Explain  : 滑动验证码解决-QQ空间滑动验证码
import time
from selenium.webdriver import ActionChains

import cv2
import requests
from selenium.webdriver import Chrome
from Slide_picture_verification_code.Slide import find_pic

driver = Chrome(executable_path="chromedriver.exe")
driver.implicitly_wait(20)
driver.get("https://qzone.qq.com/")
driver.maximize_window()

# 切换到iframe中
driver.switch_to.frame(driver.find_element_by_id('login_frame'))

# 点击账号密码登录
driver.find_element_by_id("switcher_plogin").click()
# 输入账号
account = driver.find_element_by_id("u")
account.send_keys("2590205789")
# 输入密码
pwd = driver.find_element_by_id("p")
pwd.send_keys("123456789")
# 点击登录
driver.find_element_by_id("login_button").click()

driver.switch_to.frame(driver.find_element_by_id('tcaptcha_iframe'))
# 获取背景和模块图片的url
target = driver.find_element_by_id("slideBg")
template = driver.find_element_by_id("slideBlock")
src1 = target.get_attribute('src')
src2 = template.get_attribute('src')
print(src1)
print(src2)

# 使用requests下载图片
response = requests.get(src1)
# 获取的文本实际上是推按额二进制文件
img = response.content
# 将他拷贝到本地文件 w 写 b 二进制 wb 代表写入二进制文本
with open("./img/target.png", 'wb') as f:
    f.write(img)

response = requests.get(src2)
# 获取的文本实际上是推按额二进制文件
img = response.content
# 将他拷贝到本地文件 w 写 b 二进制 wb 代表写入二进制文本
with open("./img/template.png", 'wb') as f:
    f.write(img)

# 计算横坐标的偏移量
x = find_pic()
print(x)
# 先计算图片缩放比例

# 获取原图的宽度
img = cv2.imread('./img/target.png')
w1 = img.shape[1]
# 获取网页图片显示宽度
w2 = target.size['width']

# 缩放之后的x
x = x * w2 / w1
x = int(x + 9.3 - 32)
print(x)

action = ActionChains(driver)
# 先按住像素
action.click_and_hold(template).perform()
# 拖动像素
action.move_by_offset(x, 0)
# 松开鼠标
action.release().perform()

time.sleep(5)
driver.close()
