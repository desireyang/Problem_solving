# -*- coding utf-8 -*-
# @Time    : 2020/7/21 23:33
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : key_conf.py
# Software : PyCharm
# Explain  : 存放百度AI Key值

grant_type = 'client_credentials',  # 固定值
client_id = 'API Key',  # 在开放平台注册后所建立的应用的 API key（需自行更改）
client_secret = 'Secret Key'  # 所建立应用的Secret Key（需自行更改）
