# -*- coding utf-8 -*-
# @Time    : 2020/7/21 21:51
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : __init__.py
# Software : PyCharm
# Explain  : 照片转化为动漫模式

"""
API接口文档：
    https://cloud.baidu.com/doc/IMAGEPROCESS/index.html


注意：使用需要更改key_conf.py文件中两个参数（改成自己百度AI中申请的key）：
    client_id、client_secret
"""
