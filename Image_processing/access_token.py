# -*- coding utf-8 -*-
# @Time    : 2020/7/21 22:25
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : access_token.py
# Software : PyCharm
# Explain  : 百度AI开放平台认证

import requests
from Image_processing.key_conf import (grant_type,
                                       client_id,
                                       client_secret)


def get_access_token():
    url = 'https://aip.baidubce.com/oauth/2.0/token'
    data = {
        'grant_type': grant_type,
        'client_id': client_id,
        'client_secret': client_secret
    }
    res = requests.post(url, data=data)
    res = res.json()
    # print(res)
    access_token = res['access_token']
    return access_token

# access_key = get_access_token()
# print(access_key)
