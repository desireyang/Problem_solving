# -*- coding utf-8 -*-
# @Time    : 2020/7/21 22:30
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : main.py
# Software : PyCharm
# Explain  : 主函数调用

from Image_processing.config import get_config
from Image_processing.image_process import image_process

if __name__ == '__main__':
    # 选择输入信息
    img_before, process_action, others = get_config()
    img_after = img_before.split('.')  # 将原文件名分成列表
    img_after = img_after[0] + '_1.' + img_after[1]  # 新生成的文件名为原文件名上加 _1
    image_process(img_before, img_after, process_action, others)
    print('done!')
