# -*- coding utf-8 -*-
# @Time    : 2020/7/21 22:28
# @Author  : DesireYang
# @Email   : yangyin1106@163.com
# @File    : image_process.py
# Software : PyCharm
# Explain  : 获取到图片和处理参数之后，就拿着这些信息去请求百度AI开放平台了

import base64
import requests
from Image_processing.access_token import get_access_token


def image_process(img_before, img_after, how_to_deal, others):
    # 函数的三个参数，一个是转化前的文件名，一个是转化后的文件名，均在同一目录下，第三个是图像处理能力选择
    request_url = 'https://aip.baidubce.com/rest/2.0/image-process/v1/' + how_to_deal

    file = open(img_before, 'rb')  # 二进制读取图片
    origin_img = base64.b64encode(file.read())  # 将图片进行base64编码
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {
        'access_token': get_access_token(),
        'image': origin_img,
        'option': others
    }
    res = requests.post(request_url, data=data, headers=headers)
    res = res.json()
    print(res)
    if res:
        f = open(img_after, 'wb')
        after_img = res['image']
        after_img = base64.b64decode(after_img)
        f.write(after_img)
        f.close()
